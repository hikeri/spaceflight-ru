Mod Loader=Настройки модов
Mods Settings=Настройки модов
Youtube=YouTube
Get Full Version=Получить полную версию
Exit=Выход
Play=Играть
Settings=Настройки
Discord=Discord
Reddit=Reddit
Credits=Авторы
Close=Закрыть
Cancel=Отмена
Close game?=Закрыть игру?
Reset To Default=Восстановление значений по умолчанию
Window mode=Оконный режим
Camera Shake=Дрожание камеры
Conic Sections=Конические сечения
Anti Aliasing=Сглаживание
Vertical Sync=Вертикальная синхронизация
FPS=FPS
Resolution=Разрешение
Scale UI=Масштаб интерфейса
Opacity UI=Непрозрачность интерфейса
Sound=Звук
Music=Музыка
Audio Settings=Настройки звука
Video Settings=Настройки видео
Keybindings=Назначение клавиш
FXAA=FXAA
On=Вкл.
Unlimited=Неограниченный
1920x1080=1920x1080
Windowed=Оконный
Save/Load=Сохранить/Загрузить
F5=F5
F9=F9
Select all=Выбрать все
Ctrl + A=Ctrl + A
Copy/Paste=Копировать/Вставить
Ctrl + C=Ctrl + C
Ctrl + V=Ctrl + V
Duplicate=Дублировать
Ctrl + D=Ctrl + D
BackQuote=Тильда
Delete=Удалить
Rotate part=Повернуть часть
Q=Q
E=E
Flip part=Перевернуть часть
W=W
A=A
S=S
D=D
Undo=Отменить
Ctrl + Z=Ctrl + Z
Redo=Повторить
Ctrl + Y=Ctrl + Y
Toggle ignition=Переключить зажигание
Space=Пробел
Adjust throttle=Отрегулировать газ
Ctrl=Ctrl
Shift=Shift
Min/Max throttle=Мин/Макс газ
X=X
Z=Z
Turn rocket=Поворот ракеты
Toggle RCS=Переключение РСУ
R=R
Move using RCS=Перемещение с помощью РСУ
Activate stage=Активировать ступень
Enter=Enter
Toggle map=Перевернуть карту
M=M
Timewarp=Ускорение времени
Switch rocket=Сменить ракету
Toggle console=Включить консоль
F1=F1
Off=Откл.
Option A=Вариант А
Borderless=Безрамочный
Fullscreen=Полноэкранный
Scale: {{A}}\nSpecific Impulse: {{B}}x\nTank Dry Mass: {{C}}x\nEngine Mass: {{D}}x=Масштаб: {{A}}\nУдельный импульс: {{B}}x\nМасса баков с сухим топливом: {{C}}x\nМасса двигателя: {{D}}
Štefo Mai Morojna\n<Size\=55> Designer - Programmer - Artist </size>\n\nJordi van der Molen\n<Size\=55> Programmer </size>\n\nChris Christo\n<Size\=55> Programmer </size>\n\nJosh\n<Size\=55> Programmer </size>\n\nAidan\n<Size\=55> Programmer </size>\n\nAndrey Onischenko\n<Size\=55> Programmer </size>\n\nDavi Vasc\n<Size\=55> Composer </size>\n\nAshton Mills\n<size\=55> Composer </size>=Štefo Mai Morojna\n<Size\=55> Дизайнер - программист - художник </size>\n\nJordi van der Molen\n<Size\=55> Программист </size>\n\nChris Christo\n<Size\=55> Программист </size>\n\nJosh\n<Size\=55> Программист </size>\n\nAidan\n<Size\=55> Программист </size>\n\nAndrey Onischenko\n<Size\=55> Программист </size>\n\nDavi Vasc\n<Size\=55> Композитор </size>\n\nAshton Mills\n<size\=55> Композитор </size>\n\n\n\nHikeri\n<size\=55> Переводчик </size>
Create New World=Создать новый мир
World Name=Название мира
Rename=Переименовать
Main Menu=Главное меню
Mod=Мод
Download Mods=Скачать моды
Open Mods Folder=Открыть папку модов

Loading<color\=#00000000>...</color>=Загрузка<color\=#00000000>...</color>

Build New Rocket=Построить новую ракету
Resume Game=Продолжить игру
Hello World=Hello World
Exit To Space Center=В космический центр
Cheats=Читы
Share Blueprint=Поделиться чертежом
Video Tutorials=Видеоуроки
Docking Tutorial=Стыковка
Moon Tutorial=Полет на луну
Orbit Tutorial=Выход на орбиту
Example Rockets=Примеры ракет
Moon Lander=Лунный посадочный модуль
Three Stage Rocket=Трехступенчатая ракета
Two Stage Rocket=Двухступенчатая ракета
Basic Rocket=Базовая ракета
Clear=Отчистить
Move Rocket=Переместить ракету
Load Blueprint=Загрузить чертеж
Save Blueprint=Сохранить чертеж
Exit To Main Menu=В главное меню
Infinite Fuel=Бесконечное топливо
No Atmospheric Drag=Нет атмосферного сопротивления
No Heat Damage=Нет теплового урона
Part Clipping=Свободное размещение деталей
Infinite Build Area=Бесконечная площадь строительства
No Burn Marks=Нет следов горения
No Collision Damage=Нет повреждения от столкновений
No Gravity=Нет гравитации
﻿Thrust=Тяга
Button Text=Текст кнопки
Label=Этикетка
Mass=Вес
Thrust / Weight=Тяга / Вес
Parts=Части
Height=Высота
Share=Поделиться
Load=Загрузить
Launch=Запуск
Save=Сохранить
New=Создать новый чертеж
Clear build area?=Отчистить зону сборки?
Funds=Средства
Engines=Двигатели
12 Wide=12-ой ширины
10 Wide=10-ой ширины
8 Wide=8-ой ширины
6 Wide=6-ой ширины
Other=Другие
Basics=Базовые
Aerodynamics=Аэродинамические
Fairings=Обтекательные
Structural=Структурные
Load Quicksave=Загрузить сохранение
Create Quicksave=Создать сохранение
Clear debris?\n\nThis will remove all uncontrollable rockets=Удалить мусор?\n\nЭто удалит все неконтролируемые ракеты
Clear Debris=Удалить мусор
Clear debris?=Удалить мусор?
Liquid fuel:=Жидкое топливо:
Solid fuel:=Твердое топливо:
Revert To Build=Вернуться к строительству
Revert To Launch=Вернуться к запуску
Map=Карта
Help=Помощь
Height (Terrain)=Высота (местность)
Navigate To=Построить маршрут к
End Mission=Закончить миссию
Part Count=Количество частей
Switch To=Переключить на
Throttle=Мощность
Resource name=Название ресурса
Velocity=Скорость
Destroy=Уничтожить
Out of Liquid fuel=Закончилось жидкое топливо
Out of Solid fuel=Закончилось твердое топливо
Revert 3 Min=Вернуть 3 мин.
Revert 30 Sec=Вернуть 30 сек.
Engine On=Двигатель включен
Engine Off=Двигатель выключен
RCS Off=РСУ отключен
RCS On=РСУ включен
RCS=РСУ
Asset Pack=Пакет Ассетов
IGNITION=ЗАЖИГАНИЕ
Track=Отслеживать
End Navigation=Закончить построение маршрута
Focus=Фокусировать объект
Unfocus=Расфокусировать объект
[ Left Click ]   Click parts to activate them=[ ЛКМ ]   на деталь, что бы активировать её
Timewarp Here=Ускорение времени здесь

Thrust=Тяга

Challenges: {{A}}=Испытаний {{A}}

Challenges:=Испытания:
Challenges=Испытаний

Steer rocket=Повернуть ракету
Toggle throttle=Переключить зажигание
Fly with RCS=Управление РСУ
Quicksaves=Сохранения
Relaunch=Перезапустить
Would you like to relaunch the game now so that the changes take effect?=Хотели бы вы перезапустить игру сейчас, чтобы изменения вступили в силу?
Upload Blueprint=Отправить чертеж
Download Blueprint=Загрузить чертеж
Download=Скачать
Blueprint URL=URL-адрес чертежа
Copied blueprint URL to clipboard=URL-адрес чертежа скопирован в буфер обмена
Blueprints=Чертежи
Import=Импортировать
Recover=Вернуть
Mission Achievements:=Достижения миссии
Destroy Debris=Уничтожить мусор
Destroy debris?=Уничтожить мусор?
View Flight Log=Посмотреть журнал полетов
Mission Log:=Журнал полетов:
Continue Build=Продолжить строительство

Easy=Легко
Medium=Средне
Extreme=Экстрим
Mode:=Режим:
Difficulty:=Сложность:
Normal=Нормально
Hard=Сложно
Realistic=Реалистично
World Name:=Название мира:
Create World=Создать мир
Challenge=Испытание
Sandbox=Песочница
Quicksaves:=Быстрые сохранения:
Cheats:=Читы:
This will remove all uncontrollable rockets=Это удалит все неконтролируемые ракеты
Space Center=Космический центр
Continue=Продолжить
Earth=Земля
Venus=Венера
Mercury=Меркурий
Moon=Луна
Mars=Марс
Sun=Солнце
Jupiter=Юпитер
Europa=Европа
Ganymede=Ганимед
Io=Ио
Callisto=Каллисто
Deimos=Деймос
Phobos=Фобос
Captured Asteroid=Захваченный астероид
- Part Name -=- Наименование -
Launch Anyway=Все равно запустить
A parachute used for landing=Парашют, используемый для посадки
Parachute=Парашют
A small capsule, carrying one astronaut=Небольшая капсула, несущая одного астронавта
Capsule=Капсула
Heat Shield=Тепловой экран
A heat resistant shield used to survive atmospheric reentry=Теплостойкий экран, используемый для защиты от воздействия экстремальных температур
Vertical separator, used to detach empty stages=Вертикальный разделитель, используемый для отсоединения пустых ступеней
Stage Separator=Вертикальный разделитель
Horizontal separator, used for detaching side boosters=Горизонтальный разделитель, используемый для отсоединения боковых бустеров
Side Separator=Горизонтальный разделитель
A fuel tank carrying liquid fuel and liquid oxygen=Топливный бак, несущий жидкое топливо и жидкий кислород
Fuel Tank=Топливный бак
A high thrust - lower efficiency engine, normally used in the first stage of a rocket=Двигатель с низкой эффективностью, но высокой тягой. Обычно используется на первой ступени ракеты
Titan Engine=Двигатель «Титан»
Hawk Engine=Двигатель «Ястреб»
High efficiency, low thrust. Used in space when high thrust isn't a priority=Двигатель с высокой эффективностью, но с низкой тягой. Используется в пространстве, где высокая тяга не является приоритетом
Valiant Engine=Двигатель «Храбрый»
Frontier Engine=Двигатель «Рубеж»
A tiny engine used for landers=Крошечный двигатель, используемый для посадки
Kolibri Engine=Двигатель «Колибри»
A set of small directional thrusters, used for docking=Набор небольших направленных двигателей, используемых для стыковки
RCS Thruster=Ускорители РСУ
An aerodynamic nose cone, used to improve the aerodynamics of side boosters=Аэродинамический носовой конус, используемый для улучшения аэродинамики боковых бустеров
Aerodynamic Nose Cone=Аэродинамический конус
An extendable and retractable leg used for landing on the surface of moons and planets=Расширяемая выдвижная нога, используемая для приземления на поверхности спутников и планет
Landing Leg=Посадочная нога
Redstone Atlas=Редстоун Атлас
A light and strong structural part=Это довольно прочные и простые части
Structural Part=Структурные балки
An unmanned probe, used for one way missions=Беспилотный зонд, используемый для миссий в один конец
Probe=Зонд
A docking port which can be used to connect two vehicles together=Стыковочный порт, используемый для соединения двух транспортных средств вместе
Docking Port=Стыковочный порт
A low thrust engine with an incredibly high efficiency=Двигатель с низкой тягой, но с невероятно высокой эффективностью
Ion Engine=Ионный двигатель
Rover wheel used to drive on the surface of planets=Колёса используется для езды на поверхности планет
Rover Wheel=Колесо
A solar panel that generates power when extended=Солнечная панель генерирует энергию в открытом положении \n(Декоративный элемент!)
Solar Panel=Солнечная панель
An aerodynamic fairing, used to encapsulate payloads=Аэродинамический обтекатель, используемый для улучшения аэродинамики.
Fairing=Обтекатель
An aerodynamic fuselage, used to cover engines=Аэродинамический фюзеляж, используемый для покрытия плоских поверхностей двигателей шире, чем их топливные баки
Aerodynamic Fuselage=Аэродинамический фюзеляж
Launch Escape System=Система аварийного запуска
Separator=Разделитель
Fuselage=Фюзеляж
A-7 Engine=Двигатель A-7
LR-105-5 Engine=Двигатель XLR-105-5
LR-89-5 Engine=Двигатель XLR-89-5
Engine Base=База двигателя
Retro Pack=Тормозные ракеты
Adapter=Адаптер
Cover=Ракетная обложка
Side Cover=Боковая ракетная обложка
WARNING:\nThis will undo all progress since last launch=ПРЕДУПРЕЖДЕНИЕ:\nЭто действие отменит весь прогресс с момента последнего запуска ракеты
Loading...=Загрузка...
Saving...=Сохранение...
Importing...=Импортирование...
Delete world?=Удалить мир?
Recover Debris=Удалить мусор
Recover debris?=Удалить мусор?
Efficiency=Эффективность
Solid fuel=Твердое топливо
Separation force=Сила разделения
Liquid fuel=Жидкое топливо
Heat tolerance=Теплоемкость
Torque=Крутящий момент
Max deploy height=Макс развертывание высоты
Burn Time=Время сжигания

Earth {{A}}=Земля {{A}}
Venus {{A}}=Венера {{A}}
Mercury {{A}}=Меркурий {{A}}
Moon {{A}}=Луна {{A}}
Mars {{A}}=Марс {{A}}
Phobos {{A}}=Фобос {{A}}
Deimos {{A}}=Деймос {{A}}
Europa {{A}}=Европа {{A}}
Ganymede {{A}}=Ганимед {{A}}
Io {{A}}=Ио {{A}}
Callisto {{A}}=Каллисто {{A}}
Captured Asteroid {{A}}=Захваченный астероид {{A}}

<color\=#ffffff30><size\=65>Mercury Return</size><size\=15>\n\n</size>Land on the surface of Mercury, then return safely</color>=<color\=#ffffff30><size\=65>Возвращение с Меркурия</size><size\=15>\n\n</size>Приземлиться на поверхности Меркурия, затем безопасно вернуться</color>
<color\=#ffffff30><size\=65>Mercury Landing</size><size\=15>\n\n</size>Land on the surface of Mercury</color>=<color\=#ffffff30><size\=65>Посадка на Меркурий</size><size\=15>\n\n</size>Приземлиться на поверхности Меркурия</color>
<color\=#ffffff30><size\=65>Venus Landing</size><size\=15>\n\n</size>Descend through the thick atmosphere and land on the surface of Venus</color>=<color\=#ffffff30><size\=65>Посадка на Венеру</size><size\=15>\n\n</size>Спуститься через густую атмосферу и приземлиться на поверхность Венеры</color>
<color\=#ffffff30><size\=65>Venus Return</size><size\=15>\n\n</size>Land on the surface of Venus, then ascend trough the thick atmosphere and return safely</color>=<color\=#ffffff30><size\=65>Возвращение с Венеры</size><size\=15>\n\n</size>Приземлиться на поверхности Венеры, затем поднимитесь в густую атмосферу и безопасно вернитесь</color>
<size\=65>Liftoff</size><size\=15>\n\n</size>Liftoff and land safely=<size\=65>Поднять</size><size\=15>\n\n</size>Поднять и безопасно приземлиться
<color\=#ffffff30><size\=65>Reach 10km</size><size\=15>\n\n</size>Reach 10km and land safely</color>=<color\=#ffffff30><size\=65>Достичь 10 км</size><size\=15>\n\n</size>Достичь 10 км и безопасно приземлиться</color>
<color\=#ffffff30><size\=65>Reach Space</size><size\=15>\n\n</size>Reach 30km, then survive reentry and land safely</color>=<color\=#ffffff30><size\=65>Добраться до космоса</size><size\=15>\n\n</size>Достичь 30 км, затем выжить в возвращении и безопасно приземлиться</color>
<color\=#ffffff30><size\=65>Land 100km downrange</size><size\=15>\n\n</size>Land at least 100km away from the launch pad</color>=<color\=#ffffff30><size\=65>100 км от дома</size><size\=15>\n\n</size>Приземлиться как минимум в 100 км от стартовой площадки</color>
<color\=#ffffff30><size\=65>Reach Low Earth Orbit</size><size\=15>\n\n</size>Reach low Earth orbit, then land safely</color>=<color\=#ffffff30><size\=65>Добраться до низкой орбиты с землей</size><size\=15>\n\n</size>Добраться до орбиты с низкой землей, затем приземлиться</color>
<color\=#ffffff30><size\=65>Reach High Earth Orbit</size><size\=15>\n\n</size>Reach high Earth orbit, then land safely</color>=<color\=#ffffff30><size\=65>Добраться до высокой орбиты с землей</size><size\=15>\n\n</size>Добраться до орбиты с высокой землей, затем приземлиться</color>
<color\=#ffffff30><size\=65>Moon Orbit</size><size\=15>\n\n</size>Capture into low Moon orbit, then return safely</color>=<color\=#ffffff30><size\=65>Лунная орбита</size><size\=15>\n\n</size>Запечатлеть на орбиту с низкой луной, затем вернитесь безопасно</color>
<color\=#ffffff30><size\=65>Moon Landing</size><size\=15>\n\n</size>Land on the surface of Moon, then return safely</color>=<color\=#ffffff30><size\=65>Посадка на Луну</size><size\=15>\n\n</size>Приземлиться на поверхности Луны, затем безопасно вернуться</color>
<color\=#ffffff30><size\=65>Moon Tour</size><size\=15>\n\n</size>Land on 3 separate landmarks, then return safely</color>=<color\=#ffffff30><size\=65>Лунный тур</size><size\=15>\n\n</size>Приземлиться на 3х отдельных достопримечательностях, а затем безопасно вернуться</color>
<color\=#ffffff30><size\=65>Asteroid Impact</size><size\=15>\n\n</size>Crash into the surface of the Captured Asteroid at 200+ m/s</color>=<color\=#ffffff30><size\=65>Воздействие астероидов</size><size\=15>\n\n</size>Врезаться в поверхность захваченного астероида при более чем 200 м/с/с</color>
<color\=#ffffff30><size\=65>Captured Asteroid Landing</size><size\=15>\n\n</size>Land on the surface of Captured Asteroid, then return safely</color>=<color\=#ffffff30><size\=65>Посадка на Захваченный астероид</size><size\=15>\n\n</size>Приземлиться на поверхности захваченного астероида, затем безопасно вернуться</color>
<color\=#ffffff30><size\=65>Mars Landing</size><size\=15>\n\n</size>Land on the surface of Mars, then return safely</color>=<color\=#ffffff30><size\=65>Посадка на Марс</size><size\=15>\n\n</size>Приземлиться на поверхности Марса, затем безопасно вернуться</color>
<color\=#ffffff30><size\=65>Mars Grand Tour</size><size\=15>\n\n</size>Land on Mars, Phobos and Deimos in one flight, then return safely</color>=<color\=#ffffff30><size\=65>Марс Гранд Тур</size><size\=15>\n\n</size>Приземлиться на Марсе, Фобос и Деймос в одном полете, затем безопасно вернуться</color>
<color\=#ffffff30><size\=65>Phobos Landing</size><size\=15>\n\n</size>Land on the surface of Phobos, then return safely</color>=<color\=#ffffff30><size\=65>Посадка на Фобос</size><size\=15>\n\n</size>Приземлиться на поверхности фобоса, затем безопасно вернуться</color>
<color\=#ffffff30><size\=65>Deimos Landing</size><size\=15>\n\n</size>Land on the surface of Deimos, then return safely</color>=<color\=#ffffff30><size\=65>Посадка на Деймос</size><size\=15>\n\n</size>Приземлиться на поверхности Деймоса, затем безопасно вернуться</color>
<color\=#ffffff30><size\=65>Io Landing</size><size\=15>\n\n</size>Land on the surface of Io, then return safely</color>=<color\=#ffffff30><size\=65>Посадка на Ио</size><size\=15>\n\n</size>Приземлиться на поверхности ио, затем безопасно вернуться</color>
<color\=#ffffff30><size\=65>Europa Landing</size><size\=15>\n\n</size>Land on the surface of Europa, then return safely</color>=<color\=#ffffff30><size\=65>Посадка на Европу</size><size\=15>\n\n</size>Приземлиться на поверхности Европы, затем безопасно вернуться</color>
<color\=#ffffff30><size\=65>Ganymede Landing</size><size\=15>\n\n</size>Land on the surface of Ganymede, then return safely</color>=<color\=#ffffff30><size\=65>Посадка на Ганимед</size><size\=15>\n\n</size>Приземлиться на поверхности ганимеда, затем безопасно вернуться</color>
<color\=#ffffff30><size\=65>Callisto Landing</size><size\=15>\n\n</size>Land on the surface of Callisto, then return safely</color>=<color\=#ffffff30><size\=65>Посадка на Каллисто</size><size\=15>\n\n</size>Приземлиться на поверхности Каллисто, затем безопасно вернуться</color>

Time acceleration {{A}}x=Ускорение времени {{A}}x

<Size\=70> Štefo Mai Morojna </size>\n<Size\=55> Designer - Programmer - Artist </size>\n\n<Size\=70> Jordi van der Molen </size>\n<Size\=55> Programmer </size>\n\n<Size\=70> Chris Christo </size>\n<Size\=55> Programmer </size>\n\n<Size\=70> Josh </size>\n<Size\=55> Programmer </size>\n\n<Size\=70> Aidan Ginise </size>\n<Size\=55> Programmer </size>\n\n<Size\=70> Andrey Onischenko </size>\n<Size\=55> Programmer </size>\n\n<Size\=70> Davi Vasc </size>\n<Size\=55> Composer </size>\n\n<Size\=70> Ashton Mills </size>\n<size\=55> Composer </size>=<Size\=70> Štefo Mai Morojna </size>\n<Size\=55> Дизайнер - программист - художник </size>\n\n<Size\=70> Jordi van der Molen </size>\n<Size\=55> Программист </size>\n\n<Size\=70> Chris Christo </size>\n<Size\=55> Программист </size>\n\n<Size\=70> Josh </size>\n<Size\=55> Программист </size>\n\n<Size\=70> Aidan Ginise </size>\n<Size\=55> Программист </size>\n\n<Size\=70> Andrey Onischenko </size>\n<Size\=55> Программист </size>\n\n<Size\=70> Davi Vasc </size>\n<Size\=55> Композитор </size>\n\n<Size\=70> Ashton Mills </size>\n<size\=55> Композитор </size>\n\n<Size\=65> Hikeri </size>\n<size\=45> Переводчик </size>